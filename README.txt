INTRODUCTION
-----------
This module adds a new field type 'crossword_field' with its formatter
and renders this field as a crossword


 * For a full description of the module, visit the project page:
   https://drupal.org/project/crossword_generator


 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/crossword_generator


REQUIREMENTS
------------
Drupal 7.x

INSTALLATION
------------
1. Move the entire "crossword_generator" directory to
   the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in "Administer" -> "Modules"

3. Add a new multi value field of type "crossword_field" to any content type in
   "Manage fields"

4. Create a new content with the "crossword_field" field
   and add words with clues.

CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.

MAINTAINERS
-----------
Current maintainers:
 * Andrzej Wieczorkiewicz (marabak) - https://drupal.org/user/1625476
