/**
 * @file
 * Crossword generator module javascript behaviors file.
 */

(function ($) {
  Drupal.behaviors.crossword_generator = {};
  Drupal.behaviors.crossword_generator.attach = function (context, settings) {

    var $tag = jQuery('.field-type-crossword-field >.field-items');
    if (0 < $tag.length && crosswordGenerator == null) {
      var crosswordGenerator = new CrosswordGenerator($tag);
      crosswordGenerator.create();
      crosswordGenerator.play();
    }

  };

}(jQuery));
