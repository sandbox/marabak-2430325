/**
 * @file
 * CrosswordGenerator class file.
 */

function CrosswordGenerator(tag) {
  this.$tag = jQuery(tag);
  this.board;
  this.wordArr;
  this.wordBank;
  this.wordsActive;
  this.mode;
  this.bounds = {top: 0, right: 0, bottom: 0, left: 0};

  this.$crossword = jQuery('<div class="crossword"></div>');
  this.$tag.prepend(this.$crossword);

  this.$crossword.onfocus = function () {
    return false;
  }

}

CrosswordGenerator.prototype.updateBounds = function (x, y) {
  this.bounds.top = Math.min(y, this.bounds.top);
  this.bounds.right = Math.max(x, this.bounds.right);
  this.bounds.bottom = Math.max(y, this.bounds.bottom);
  this.bounds.left = Math.min(x, this.bounds.left);
};

CrosswordGenerator.prototype.cleanBounds = function () {
  this.bounds.top = 999;
  this.bounds.right = 0;
  this.bounds.bottom = 0;
  this.bounds.left = 999;
};

CrosswordGenerator.prototype.play = function () {
  var letters = this.$tag.find('.letter');

  for (var i = 0; i < letters.length; i++) {
    letters[i].innerHTML = "<input class='char' type='text' maxlength='1'></input>";
  }
  this.mode = 0;
  this.toggleInputBoxes(false);
};

CrosswordGenerator.prototype.create = function () {
  if (this.mode === 0) {
    this.toggleInputBoxes(true);
    this.$crossword.html(this.boardToHtml(" "));
    this.mode = 1;
  }
  else {
    this.getWordsFromInput();
    for (var i = 0, isSuccess = false; i < 100 && !isSuccess; i++) {
      this.cleanVars();
      isSuccess = this.populateBoard();
    }
    this.$crossword.html((isSuccess) ? this.boardToHtml(" ") : "Failed to find crossword.");
  }
};

CrosswordGenerator.prototype.toggleInputBoxes = function (active) {
  var w = this.$tag.find('.word');
  var d = this.$tag.find('.clue');

  for (var i = 0; i < w.length; i++) {
    if (active === true) {
      jQuery(w[i]).removeClass('hide');
      jQuery(d[i]).removeClass('clue-read-only');
      d[i].disabled = '';
    }
    else {
      jQuery(w[i]).addClass('hide');
      jQuery(d[i]).addClass('clue-read-only');
      d[i].disabled = 'readonly';
    }
  }
};

CrosswordGenerator.prototype.getWordsFromInput = function (active) {
  this.wordArr = [];
  for (var i = 0, val, w = this.$tag.find(".word"); i < w.length; i++) {
    val = jQuery(w[i]).html().toUpperCase();
    if (val !== null && val.length > 1) {
      this.wordArr.push(val);
    }
  }
};

CrosswordGenerator.prototype.cleanVars = function (active) {
  this.cleanBounds();
  this.wordBank = [];
  this.wordsActive = [];
  this.board = [];

  for (var i = 0; i < 32; i++) {
    this.board.push([]);
    for (var j = 0; j < 32; j++) {
      this.board[i].push(null);
    }
  }
};

CrosswordGenerator.prototype.populateBoard = function (active) {
  this.prepareBoard();

  for (var i = 0, isOk = true, len = this.wordBank.length; i < len && isOk; i++) {
    isOk = this.addWordToBoard();
  }
  return isOk;
};

CrosswordGenerator.prototype.prepareBoard = function () {
  this.wordBank = [];

  for (var i = 0, len = this.wordArr.length; i < len; i++) {
    this.wordBank.push(this.getWordObject(this.wordArr[i]));
  }

  for (i = 0; i < this.wordBank.length; i++) {
    for (var j = 0, wA = this.wordBank[i]; j < wA.char.length; j++) {
      for (var k = 0, cA = wA.char[j]; k < this.wordBank.length; k++) {
        for (var l = 0, wB = this.wordBank[k]; k !== i && l < wB.char.length; l++) {
          wA.totalMatches += (cA === wB.char[l]) ? 1 : 0;
        }
      }
    }
  }
};

CrosswordGenerator.prototype.addWordToBoard = function () {
  var i, len, curIndex, curWord, curChar, curMatch, testWord, testChar,
    minMatchDiff = 9999, curMatchDiff;

  if (this.wordsActive.length < 1) {
    curIndex = 0;
    for (i = 0, len = this.wordBank.length; i < len; i++) {
      if (this.wordBank[i].totalMatches < this.wordBank[curIndex].totalMatches) {
        curIndex = i;
      }
    }
    this.wordBank[curIndex].successfulMatches = [{x: 12, y: 12, dir: 0}];
  }
  else {
    curIndex = -1;

    for (i = 0, len = this.wordBank.length; i < len; i++) {
      curWord = this.wordBank[i];
      curWord.effectiveMatches = 0;
      curWord.successfulMatches = [];

      for (var j = 0, lenJ = curWord.char.length; j < lenJ; j++) {
        curChar = curWord.char[j];
        for (var k = 0, lenK = this.wordsActive.length; k < lenK; k++) {
          testWord = this.wordsActive[k];
          for (var l = 0, lenL = testWord.char.length; l < lenL; l++) {
            testChar = testWord.char[l];
            if (curChar === testChar) {
              curWord.effectiveMatches++;

              var curCross = {x: testWord.x, y: testWord.y, dir: 0};
              if (testWord.dir === 0) {
                curCross.dir = 1;
                curCross.x += l;
                curCross.y -= j;
              }
              else {
                curCross.dir = 0;
                curCross.y += l;
                curCross.x -= j;
              }

              var isMatch = true;

              for (var m = -1, lenM = curWord.char.length + 1; m < lenM; m++) {
                var crossVal = [];
                if (m !== j) {
                  if (curCross.dir === 0) {
                    var xIndex = curCross.x + m;

                    if (xIndex < 0 || xIndex > this.board.length) {
                      isMatch = false;
                      break;
                    }

                    crossVal.push(this.board[xIndex][curCross.y]);
                    crossVal.push(this.board[xIndex][curCross.y + 1]);
                    crossVal.push(this.board[xIndex][curCross.y - 1]);
                  }
                  else {
                    var yIndex = curCross.y + m;

                    if (yIndex < 0 || yIndex > this.board[curCross.x].length) {
                      isMatch = false;
                      break;
                    }

                    crossVal.push(this.board[curCross.x][yIndex]);
                    crossVal.push(this.board[curCross.x + 1][yIndex]);
                    crossVal.push(this.board[curCross.x - 1][yIndex]);
                  }

                  if (m > -1 && m < lenM - 1) {
                    if (crossVal[0] !== curWord.char[m]) {
                      if (crossVal[0] !== null) {
                        isMatch = false;
                        break;
                      }
                      else {
                        if (crossVal[1] !== null) {
                          isMatch = false;
                          break;
                        }
                        else {
                          if (crossVal[2] !== null) {
                            isMatch = false;
                            break;
                          }
                        }
                      }
                    }
                  }
                  else {
                    if (crossVal[0] !== null) {
                      isMatch = false;
                      break;
                    }
                  }
                }
              }

              if (isMatch === true) {
                curWord.successfulMatches.push(curCross);
              }
            }
          }
        }
      }

      curMatchDiff = curWord.totalMatches - curWord.effectiveMatches;

      if (curMatchDiff < minMatchDiff && curWord.successfulMatches.length > 0) {
        curMatchDiff = minMatchDiff;
        curIndex = i;
      }
      else {
        if (curMatchDiff <= 0) {
          return false;
        }
      }
    }
  }

  if (curIndex === -1) {
    return false;
  }

  var spliced = this.wordBank.splice(curIndex, 1);
  this.wordsActive.push(spliced[0]);

  var pushIndex = this.wordsActive.length - 1,
    rand = Math.random(),
    matchArr = this.wordsActive[pushIndex].successfulMatches,
    matchIndex = Math.floor(rand * matchArr.length),
    matchData = matchArr[matchIndex];

  this.wordsActive[pushIndex].x = matchData.x;
  this.wordsActive[pushIndex].y = matchData.y;
  this.wordsActive[pushIndex].dir = matchData.dir;

  for (i = 0, len = this.wordsActive[pushIndex].char.length; i < len; i++) {
    var xIndex = matchData.x,
      yIndex = matchData.y;

    if (i == 0) {
      var num = 1 + this.wordArr.indexOf(this.wordsActive[pushIndex].string);
      if (matchData.dir === 0) {
        this.board[xIndex - 1][yIndex] = {'num': num};
      }
      else {
        this.board[xIndex][yIndex - 1] = {'num': num};
      }
    }

    if (matchData.dir === 0) {
      xIndex += i;
      this.board[xIndex][yIndex] = {'char': this.wordsActive[pushIndex].char[i]};
    }
    else {
      yIndex += i;
      this.board[xIndex][yIndex] = {'char': this.wordsActive[pushIndex].char[i]};
    }

    this.updateBounds(xIndex, yIndex);
  }

  return true;
};

CrosswordGenerator.prototype.boardToHtml = function (blank) {
  for (var i = this.bounds.top - 1, str = ""; i < this.bounds.bottom + 2; i++) {
    str += "<div class='row'>";
    for (var j = this.bounds.left - 1; j < this.bounds.right + 2; j++) {
      str += this.boardCharToElement(this.board[j][i]);
    }
    str += "</div>";
  }
  return str;
};

CrosswordGenerator.prototype.boardCharToElement = function (c) {
  if (c) {
    if (c.num) {
      return this.eleStr('div', [{a: 'class', v: ['square', 'num']}], c.num);
    }
    else {
      var arr = c.char ? ['square', 'letter'] : ['square'];
      return this.eleStr('div', [{a: 'class', v: arr}], c.char);
    }
  }
  else {
    return this.eleStr('div', [{a: 'class', v: ['square']}], null);
  }
};

CrosswordGenerator.prototype.eleStr = function (e, c, h) {
  h = (h) ? h : "";
  for (var i = 0, s = "<" + e + " "; i < c.length; i++) {
    s += c[i].a + "='" + this.arrayToString(c[i].v, " ") + "' ";
  }
  return (s + ">" + h + "</" + e + ">");
};

CrosswordGenerator.prototype.arrayToString = function (a, s) {
  if (a === null || a.length < 1) {
    return "";
  }
  if (s === null) {
    s = ",";
  }
  for (var r = a[0], i = 1; i < a.length; i++) {
    r += s + a[i];
  }
  return r;
};

CrosswordGenerator.prototype.getWordObject = function (stringValue) {
  return {
    'string': stringValue,
    'char': stringValue.split(""),
    'totalMatches': 0,
    'effectiveMatches': 0,
    'successfulMatches': []
  };
};
